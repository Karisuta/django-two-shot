from django.urls import path
from accounts.views import user_login, logout_view, signup


urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", logout_view, name="logout"),
    path("signup/", signup, name="signup"),
]
